import React, { FunctionComponent } from 'react';
import style from './style.module.css';
import { UsersComponent } from '../users/component';

export const AppComponent: FunctionComponent = () => {
  return <div className={style.app} data-testid='app'>
    <UsersComponent />
  </div>;
};
