import React from 'react';
import { render, RenderResult } from '@testing-library/react';
import { AppComponent } from './component';

describe('AppComponent', () => {
  let renderResult: RenderResult;

  beforeEach(() => {
    renderResult = render(<AppComponent />);
  });

  it('should render AppComponent', () => {
    const element = renderResult.getByTestId('app');
    expect(element).toBeInTheDocument();
  });

  it('should render UsersComponent inside the app', () => {
    const element = renderResult.getByTestId('users');
    expect(element).toBeInTheDocument();
  });
});
