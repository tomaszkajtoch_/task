import axios from 'axios';
import AxiosMockAdapter from 'axios-mock-adapter';
import React from 'react';
import { act, fireEvent, render, waitForElement } from '@testing-library/react';
import usersMock from './mock/users.mock.json';
import { UsersComponent } from './component';

describe('UsersComponent', () => {
  let axiosMockAdapter;

  beforeAll(() => {
    axiosMockAdapter = new AxiosMockAdapter(axios);

    axiosMockAdapter.onGet('https://jsonplaceholder.typicode.com/users').reply(200, usersMock);
  });

  it('should render UsersListItemComponent', async () => {
    const renderResult = render(<UsersComponent />);

    const element = renderResult.getByTestId('users');
    expect(element).toBeInTheDocument();

    await renderResult.findByTestId('users-list');
  });

  it('should support filtering', async () => {
    const renderResult = render(<UsersComponent />);

    const element = renderResult.getByTestId('users');
    expect(element).toBeInTheDocument();

    await renderResult.findByTestId('users-list');

    expect(await renderResult.findAllByTestId('users-list-item')).toHaveLength(2);

    const inputElement = renderResult.getByTestId('users-search-input');
    act(() => {
      fireEvent.change(inputElement, { target: { value: 'leanne' }})
    });

    await waitForElement(() => renderResult.findByTestId('users-list'));

    expect(await renderResult.findAllByTestId('users-list-item')).toHaveLength(1);
  });
});
