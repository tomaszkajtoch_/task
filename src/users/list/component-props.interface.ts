import { UsersUserModelInterface } from '../user-model.interface';

export interface UsersListComponentPropsInterface {
  items: Array<UsersUserModelInterface>;
}
