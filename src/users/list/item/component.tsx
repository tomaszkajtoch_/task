import React, { FunctionComponent } from 'react';
import { UsersListItemComponentPropsInterface } from './component-props.interface';
import styles from './style.module.css';

export const UsersListItemComponent: FunctionComponent<UsersListItemComponentPropsInterface> = ({ item }) => (
  <li className={styles.userslistitem} data-testid='users-list-item'>
    <span className={styles.userslistitem__name} data-testid='users-list-item-name'>
      {item.name}
    </span>
    <span className={styles.userslistitem__user} data-testid='users-list-item-user'>
      @{item.username}
    </span>
  </li>
);
