import { UsersUserModelInterface } from '../../user-model.interface';

export interface UsersListItemComponentPropsInterface {
  item: UsersUserModelInterface;
}
