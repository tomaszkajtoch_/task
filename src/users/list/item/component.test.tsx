import React from 'react';
import { render, RenderResult } from '@testing-library/react';
import { UsersListItemComponent } from './component';
import { UsersListItemComponentPropsInterface } from './component-props.interface';

describe('UsersListItemComponent', () => {
  let props: UsersListItemComponentPropsInterface;
  let renderResult: RenderResult;

  beforeEach(() => {
    props = {
      item: {
        name: 'name',
        username: 'username',
        email: 'email',
        id: 1,
        address: {
          city: 'city',
          company: {
            name: 'name',
            bs: 'bs',
            catchPhrase: 'catchPhrase',
          },
          geo: {
            lat: 'lat',
            lng: 'lng',
          },
          phone: 'phone',
          street: 'street',
          suite: 'suite',
          website: 'website',
          zipcode: 'zipcode',
        },
      },
    };

    renderResult = render(<UsersListItemComponent {...props} />);
  });

  it('should render UsersListItemComponent', () => {
    const element = renderResult.getByTestId('users-list-item');
    expect(element).toBeInTheDocument();
  });

  it('should render name of the user', () => {
    const element = renderResult.getByTestId('users-list-item-name');
    expect(element).toHaveTextContent('name');
  });

  it('should render username of the user', () => {
    const element = renderResult.getByTestId('users-list-item-user');
    expect(element).toHaveTextContent('user');
  });
});
