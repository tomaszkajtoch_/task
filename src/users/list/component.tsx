import React, { FunctionComponent } from 'react';
import { UsersListItemComponent } from './item/component';
import { UsersListComponentPropsInterface } from './component-props.interface';
import styles from './style.module.css';

export const UsersListComponent: FunctionComponent<UsersListComponentPropsInterface> = ({ items }) => (
  <ul className={styles.userslist} data-testid='users-list'>
    {items.map((item, index) => <UsersListItemComponent item={item} key={item.id} />)}
  </ul>
);
