import React from 'react';
import { render, RenderResult } from '@testing-library/react';
import usersMock from '../mock/users.mock.json';
import { UsersListComponent } from './component';
import { UsersListComponentPropsInterface } from './component-props.interface';

describe('UsersListComponent', () => {
  let props: UsersListComponentPropsInterface;
  let renderResult: RenderResult;

  beforeEach(() => {
    props = {
      items: [],
    };
    renderResult = render(<UsersListComponent {...props} />);
  });

  it('should render UsersListComponent', () => {
    const component = renderResult.getByTestId('users-list');
    expect(component).toBeInTheDocument();
  });

  it('should render UsersListItemComponent components as children', () => {
    renderResult.rerender(<UsersListComponent {...props} items={usersMock} />);
    const items = renderResult.getAllByTestId('users-list-item');
    expect(items).toHaveLength(2);
  });
});
