import axios from 'axios';
import React, { useEffect, useState, FunctionComponent } from 'react';
import { UsersUserModelInterface } from './user-model.interface';
import { UsersListComponent } from './list/component';
import { UsersSearchInputComponent } from './search-input/component';
import { usersFilter } from './filter';
import styles from './style.module.css';

export const UsersComponent: FunctionComponent = () => {
  const [ users, setUsers ] = useState<Array<UsersUserModelInterface>>([]);
  const [ searchValue, setSearchValue ] = useState<string>('');

  useEffect(() => {
    const cancelToken = axios.CancelToken.source();

    (async () => {
      try {
        const response = await axios.get<Array<UsersUserModelInterface>>(
          'https://jsonplaceholder.typicode.com/users',
          {
            cancelToken: cancelToken.token,
          },
        );

        setUsers(response.data);
      } catch (error) {
        // noop
        // In the real world scenario we should handle request fails gracefully
      }
    })();

    return cancelToken.cancel;
  });

  const filteredUsers = usersFilter(users, searchValue);

  return (
    <div className={styles.users} data-testid='users'>
      <h1 className={styles.users__title}>Users list</h1>
      <UsersSearchInputComponent value={searchValue} onChange={setSearchValue} />
      {filteredUsers && <UsersListComponent items={filteredUsers} />}
    </div>
  );
};
