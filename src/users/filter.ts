import { UsersUserModelInterface } from './user-model.interface';

export function usersFilter<T extends UsersUserModelInterface = UsersUserModelInterface>(
  users: Array<T>,
  name: string,
): Array<T> {
  name = name.toLowerCase();

  return users.filter((item) => item.name.toLowerCase().includes(name));
}
