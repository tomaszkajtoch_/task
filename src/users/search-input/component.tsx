import React, { FunctionComponent } from 'react';
import { UsersSearchInputComponentPropsInterface } from './component-props.interface';
import styles from './style.module.css';

export const UsersSearchInputComponent: FunctionComponent<UsersSearchInputComponentPropsInterface> = ({
  value,
  onChange,
}) => (
  <input
    type='text'
    className={styles.userssearchinput}
    value={value}
    onChange={(event) => onChange(event.target.value)}
    data-testid='users-search-input'
  />
);
