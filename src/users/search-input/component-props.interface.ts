export interface UsersSearchInputComponentPropsInterface {
  value: string;
  onChange: (newValue: string) => void;
}
