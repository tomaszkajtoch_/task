import React from 'react';
import { render, RenderResult, fireEvent } from '@testing-library/react';
import { UsersSearchInputComponent } from './component';
import { UsersSearchInputComponentPropsInterface } from './component-props.interface';

describe('UsersSearchInputComponent', () => {
  let props: UsersSearchInputComponentPropsInterface;
  let renderResult: RenderResult;

  beforeEach(() => {
    props = {
      value: '',
      onChange: jest.fn(),
    };
    renderResult = render(<UsersSearchInputComponent {...props} />);
  });

  it('should render UsersSearchInputComponent', () => {
    const searchInput = renderResult.getByTestId('users-search-input');
    expect(searchInput).toBeInTheDocument();
  });

  it('should have a specified value from props', () => {
    expect(renderResult.getByTestId('users-search-input')).toHaveValue(props.value);
    renderResult.rerender(<UsersSearchInputComponent {...props} value='mock' />);
    expect(renderResult.getByTestId('users-search-input')).toHaveValue('mock');
  });

  it('should call onChange when value changes', () => {
    const element = renderResult.getByTestId('users-search-input');
    expect(props.onChange).not.toBeCalled();

    fireEvent.change(element, { target: { value: 'mock' } });
    expect(props.onChange).toBeCalledWith('mock');
  });
});
